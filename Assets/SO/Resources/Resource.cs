using UnityEngine;

namespace CastleRush.SO.Resources
{
    [CreateAssetMenu(menuName = "SO/Resources/Resource", fileName = "SampleResource")]
    public class Resource : ScriptableObject
    {
        public Sprite Icon;
        public string Name;
    }
}