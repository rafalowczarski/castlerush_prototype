﻿using CastleRush.SO.Resources;
using UnityEngine;

namespace CastleRush.SO.Building
{
    [CreateAssetMenu(menuName = "SO/Buildings/BuildingSetup", fileName = "BuildingSetup")]
    public class BuildingSetup : ScriptableObject
    {
        public string Name;
        public Resource Resource;
        public GameObject Model;
        public Sprite Icon;
    }
}
