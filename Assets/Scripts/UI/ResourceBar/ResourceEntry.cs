using CastleRush.Resources;
using CastleRush.Resources.Signals;
using CastleRush.SO.Resources;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace CastleRush.UI.ResourceBar
{
    public class ResourceEntry : MonoBehaviour
    {
        [Inject] SignalBus _signalBus;
        [Inject] ResourcesSystem _resourcesSystem;

        [SerializeField] private TMP_Text amountText;
        [SerializeField] private Image icon;

        private Resource _resource;
        public void Initialize(Resource resource)
        {
            _resource = resource;
            icon.sprite = resource.Icon;

            _signalBus.Subscribe<ResourceUpdatedSignal>(FetchResource);

            FetchResource();
        }

        private void FetchResource()
        {
            int amount = _resourcesSystem.GetAmount(_resource);
            amountText.text = $"{amount}";
        }
    }
}