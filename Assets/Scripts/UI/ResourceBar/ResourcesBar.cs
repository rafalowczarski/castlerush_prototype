﻿using CastleRush.Resources;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace CastleRush.UI.ResourceBar
{
    public class ResourcesBar:MonoBehaviour
    {
        [Inject] ResourcesSystem _resourcesSystem;
        [Inject] DiContainer _diContainer;
        [SerializeField] private GameObject resourceEntry;
        [SerializeField] private Transform container;

        private void Start()
        {
            InitializeBar();
        }

        private void InitializeBar()
        {
            List<ResourceData> resourcesData = _resourcesSystem.GetResources();

            foreach(ResourceData resource in resourcesData)
            {
                GameObject entry = _diContainer.InstantiatePrefab(resourceEntry, container);
                entry.GetComponent<ResourceEntry>().Initialize(resource.Resource);
            }
        }
    }
}
