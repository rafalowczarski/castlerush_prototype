using CastleRush.UI.Popup.Signals;
using UnityEngine;
using Zenject;

namespace CastleRush.UI.Popup
{
    public class PopupInstaller : MonoInstaller<PopupInstaller>
    {
        [SerializeField] private PopupSystem.Data popupsData;
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<PopupSystem>().AsSingle().NonLazy();
            Container.BindInstance(popupsData);
            Container.DeclareSignal<OnClosePopupSignal>();
        }
    }
}