﻿namespace CastleRush.UI.Popup.Signals
{
    internal class OnBuildingSelectionUpdatedSignal
    {
        public bool Value { get; }

        public OnBuildingSelectionUpdatedSignal(bool value)
        {
            this.Value = value;
        }
    }
}
