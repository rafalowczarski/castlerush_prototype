﻿using CastleRush.SO.Building;

namespace CastleRush.UI.Popup.Signals
{
    public class OnBuildingSelectedSignal
    {
        public BuildingSetup setup { get; }

        public OnBuildingSelectedSignal(BuildingSetup setup)
        {
            this.setup = setup;
        }
    }
}
