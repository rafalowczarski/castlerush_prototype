﻿using CastleRush.SO.Building;

namespace CastleRush.UI.Popup.Signals
{
    public class OnBuildingDeselectedSignal
    {
        public BuildingSetup Setup { get; }

        public OnBuildingDeselectedSignal(BuildingSetup setup)
        {
            this.Setup = setup;
        }
    }
}
