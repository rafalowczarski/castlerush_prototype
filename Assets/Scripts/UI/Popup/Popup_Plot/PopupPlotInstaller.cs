﻿using CastleRush.UI.Popup.Signals;
using Zenject;

namespace CastleRush.UI.Popup.PopupPlot
{
    public class PopupPlotInstaller : MonoInstaller<PopupPlotInstaller>
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<OnBuildingSelectedSignal>();
            Container.DeclareSignal<OnBuildingDeselectedSignal>();
            Container.DeclareSignal<OnBuildingSelectionUpdatedSignal>();
            Container.BindInterfacesAndSelfTo<BuildingSelection>().AsSingle().NonLazy();
        }
    }
}
