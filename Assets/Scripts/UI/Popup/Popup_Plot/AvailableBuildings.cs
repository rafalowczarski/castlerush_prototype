using CastleRush.Buildings;
using CastleRush.SO.Building;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace CastleRush.UI.Popup.PopupPlot
{
    public class AvailableBuildings : MonoBehaviour
    {
        [Inject] BuildingSetupsProvider _buildingSetupsProvider;
        [Inject] DiContainer _diContainer;

        [SerializeField] private GameObject listEntryPrefab;
        [SerializeField] private ToggleGroup toggleGroup;

        List<BuildingEntry> _views = new List<BuildingEntry>();
        private void OnEnable()
        {
            Refresh();
        }

        private void Refresh()
        {
            _views.ForEach(x => x.gameObject.SetActive(false));
            List<BuildingSetup> setups = _buildingSetupsProvider.Buildings;

            foreach (BuildingSetup setup in setups)
            {
                BuildingEntry entry = _views.Find(x => x.enabled == false);

                if (entry == null)
                {
                    entry = _diContainer.InstantiatePrefab(listEntryPrefab, this.transform).GetComponent<BuildingEntry>();
                    Toggle toggle = entry.GetComponent<Toggle>();
                    toggle.group = toggleGroup;
                    _views.Add(entry);
                }

                entry.Initialize(setup);
            }
        }
    }
}