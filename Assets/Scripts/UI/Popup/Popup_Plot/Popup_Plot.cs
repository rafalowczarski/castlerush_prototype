using System.Collections.Generic;
using Zenject;

namespace CastleRush.UI.Popup.PopupPlot
{
    public class Popup_Plot : BasePopup
    {
        [Inject] private BuildingSelection _buildingSelection;
        public override PopupType PopupType => PopupType.Plot;

        public override void Open(Dictionary<int, object> parameters = null)
        {
            this.gameObject.SetActive(true);
            _buildingSelection.Init(parameters);
        }

    }
}