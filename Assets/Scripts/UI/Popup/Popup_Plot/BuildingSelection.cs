﻿using CastleRush.Buildings.Signals;
using CastleRush.SO.Building;
using CastleRush.UI.Popup.Signals;
using System;
using System.Collections.Generic;
using Zenject;

namespace CastleRush.UI.Popup.PopupPlot
{
    public class BuildingSelection:IInitializable
    {
        public BuildingSetup SelectedBuilding { get; private set; }

        [Inject] SignalBus _signalBus;
        private int PlotId;

        public void Initialize()
        {
            _signalBus.Subscribe<OnBuildingSelectedSignal>(OnSelectedBuilding);
            _signalBus.Subscribe<OnBuildingDeselectedSignal>(OnDeselectedBuilding);
        }

        public void Init(Dictionary<int, object> parameters)
        {
            PlotId = (int)parameters[1];
        }

        private void OnSelectedBuilding(OnBuildingSelectedSignal signal)
        {
            SelectedBuilding = signal.setup;

            _signalBus.Fire(new OnBuildingSelectionUpdatedSignal(SelectedBuilding != null));
        }
        private void OnDeselectedBuilding(OnBuildingDeselectedSignal signal)
        {
            if(SelectedBuilding == signal.Setup)
            {
                SelectedBuilding = null;
            }

            _signalBus.Fire(new OnBuildingSelectionUpdatedSignal(SelectedBuilding != null));
        }

        public void Build()
        {
            _signalBus.Fire(new OnBuildingSpawnRequestSignal(PlotId, SelectedBuilding));
        }

    }
}
