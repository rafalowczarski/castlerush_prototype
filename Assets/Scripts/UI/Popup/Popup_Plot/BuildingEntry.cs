using CastleRush.SO.Building;
using CastleRush.UI.Popup.Signals;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace CastleRush.UI.Popup.PopupPlot
{
    public class BuildingEntry : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;

        [SerializeField] private Image icon;
        [SerializeField] private TMP_Text title;
        [SerializeField] private Toggle toggle;

        private BuildingSetup _setup;
        public void Initialize(BuildingSetup setup)
        {
            _setup = setup;
            icon.sprite = setup.Icon;
            title.text = setup.Name;
            toggle.onValueChanged.AddListener(OnValueChanged);
        }

        private void OnValueChanged(bool value)
        {
            if (value)
            {
                _signalBus.Fire(new OnBuildingSelectedSignal(_setup));
            }
            else
            {
                _signalBus.Fire(new OnBuildingDeselectedSignal(_setup));
            }
        }

        private void OnDisable()
        {
            toggle.onValueChanged.RemoveAllListeners();
        }
    }
}