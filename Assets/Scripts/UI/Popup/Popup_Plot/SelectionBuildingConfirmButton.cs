﻿using CastleRush.UI.Popup.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


namespace CastleRush.UI.Popup.PopupPlot
{
    public class SelectionBuildingConfirmButton:MonoBehaviour
    {
        [Inject] SignalBus _signalBus;
        [Inject] BuildingSelection _buildingSelection;
        [Inject] PopupSystem _popupSystem;

        private Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnConfirmButtonClicked);
            _signalBus.Subscribe<OnBuildingSelectionUpdatedSignal>(OnBuildingSelected);
        }

        private void OnConfirmButtonClicked()
        {
            _popupSystem.CloseAll();
            _buildingSelection.Build();

        }

        private void OnEnable()
        {
            _button.interactable = false;
        }
        private void OnBuildingSelected(OnBuildingSelectionUpdatedSignal signal)
        {
            _button.interactable = signal.Value;
        }

    }
}
