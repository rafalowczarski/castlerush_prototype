﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace CastleRush.UI.Popup
{
    public enum PopupType
    {
        Building = 1,
        Plot = 2
    }
    public class PopupSystem :  IInitializable
    {
        [Inject] Data _data;
        List<IPopupActivator> _popups = new List<IPopupActivator>();

        public void Initialize()
        {
            foreach (BasePopup popup in _data.Popups)
            {
                _popups.Add(popup);
            }
        }

        public void Open(PopupType type, Dictionary<int, object> parameters = null)
        {
            IPopupActivator popup = _popups.Find(x => x.PopupType == type);

            if(popup != null)
            {
                popup.Open(parameters);
            }
            else
            {
                Debug.Log("Popup not found : " + type.ToString());
            }
        }
        public void CloseAll()
        {
            foreach (IPopupActivator popup in _popups)
            {
                popup.Close();
            }
        }

        [Serializable]
        public class Data
        {
            public List<BasePopup> Popups = new List<BasePopup>();
        }
    }
}
