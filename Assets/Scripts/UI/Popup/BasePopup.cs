﻿using System.Collections.Generic;
using UnityEngine;

namespace CastleRush.UI.Popup
{
    public interface IPopupActivator
    {
        void Open(Dictionary<int, object> parameters = null);
        void Close();
        PopupType PopupType { get; }
    }

    public abstract class BasePopup : MonoBehaviour, IPopupActivator
    {
        public abstract PopupType PopupType { get; }

        public abstract void Open(Dictionary<int, object> parameters = null);

        public virtual void Close()
        {
            this.gameObject.SetActive(false);
        }
    }
}
