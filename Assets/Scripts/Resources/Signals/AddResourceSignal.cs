﻿using CastleRush.SO.Resources;

namespace CastleRush.Resources.Signals
{
    public class AddResourceSignal
    {
        public Resource Resource { get; }
        public AddResourceSignal(Resource resource)
        {
            this.Resource = resource;
        }
    }
}
