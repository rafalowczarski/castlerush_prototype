﻿using CastleRush.SO.Resources;

namespace CastleRush.Resources.Signals
{
    public class RemoveResourceSignal
    {
        public Resource Resource { get; }
        public RemoveResourceSignal(Resource resource)
        {
            this.Resource = resource;
        }
    }
}
