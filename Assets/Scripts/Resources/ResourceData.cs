﻿using CastleRush.SO.Resources;
using System;

namespace CastleRush.Resources
{
    [Serializable]
    public class ResourceData
    {
        public Resource Resource;
        public int Amount;
    }
}
