﻿using CastleRush.SO.Resources;
using System;
using System.Collections.Generic;
using Zenject;

namespace CastleRush.Resources
{
    public class ResourcesSystem
    {
        [Inject] Data data;

        [Serializable]
        public class Data
        {
            public List<ResourceData> Resources = new List<ResourceData>();
        }

        public int GetAmount(Resource resource)
        {
            ResourceData resourceData = data.Resources.Find(x => x.Resource == resource);

            return resourceData.Amount;
        }

        public List<ResourceData> GetResources()
        {
            return data.Resources;
        }
    }
}
