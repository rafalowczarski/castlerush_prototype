﻿using CastleRush.Resources.Signals;
using Zenject;

namespace CastleRush.Resources
{
    public class ResourcesSystemInstaller : MonoInstaller<ResourcesSystemInstaller>
    {
        public ResourcesSystem.Data ResourceSystemData;
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<ResourceUpdatedSignal>();
            Container.DeclareSignal<RemoveResourceSignal>();
            Container.DeclareSignal<AddResourceSignal>();
            Container.BindInterfacesAndSelfTo<ResourcesSystem>().AsSingle().NonLazy();
            Container.BindInstance(ResourceSystemData);
        }
    }
}
