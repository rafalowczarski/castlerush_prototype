using CastleRush.Buildings.Signals;
using UnityEngine;
using Zenject;

namespace CastleRush.Buildings
{
    public class BuildingInstaller : MonoInstaller<BuildingInstaller>
    {
        [SerializeField] private BuildingSetupsProvider.Data buildingsData;
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<PlotRegistration>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<BuildingSetupsProvider>().AsSingle().NonLazy();
            Container.BindInstance(buildingsData);
            Container.DeclareSignal<OnBuildingSpawnRequestSignal>();
        }
    }
}