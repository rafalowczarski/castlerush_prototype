using CastleRush.Interactions;
using CastleRush.UI.Popup;
using System.Collections.Generic;
using Zenject;

namespace CastleRush.Buildings
{
    public interface IBuildingPlot
    {
        void Interaction();
    }

    public class BuildingPlotInteractor : BaseInteractor
    {
        [Inject] private PopupSystem _popupSystem;

        private int plotId;

        private void Start()
        {
            plotId = GetComponentInParent<BuildingView>().PlotId;
        }
        public override void Interaction()
        {
            OpenPopup();
        }

        private void OpenPopup()
        {
            Dictionary<int, object> parameters = new Dictionary<int, object>() { { 1, plotId } };
            _popupSystem.Open(PopupType.Plot, parameters);
        }
    }
}