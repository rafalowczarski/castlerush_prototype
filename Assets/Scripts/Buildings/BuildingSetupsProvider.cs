using CastleRush.SO.Building;
using System;
using System.Collections.Generic;
using Zenject;

namespace CastleRush.Buildings
{
    public class BuildingSetupsProvider
    {
        [Inject] Data _data;
        public List<BuildingSetup> Buildings => _data.Buildings;


        [Serializable]
        public class Data
        {
            public List<BuildingSetup> Buildings;
        }
    }
}