﻿using CastleRush.SO.Building;

namespace CastleRush.Buildings.Signals
{
    internal class OnBuildingSpawnRequestSignal
    {
        public int PlotId { get; }
        public BuildingSetup Setup { get; }
        public OnBuildingSpawnRequestSignal(int plotId, BuildingSetup setup)
        {
            this.PlotId = plotId;
            this.Setup = setup;
        }
    }
}
