﻿using System.Collections.Generic;

namespace CastleRush.Buildings
{
    public class PlotRegistration
    {
        private List<int> _ids = new List<int>();
        public int Register()
        {
            int result = 0;

            while(true)
            {
                if(_ids.Contains(result))
                {
                    result++;
                    continue;
                }
                _ids.Add(result);
                break;
            }

            return result;
        }
    }
}
