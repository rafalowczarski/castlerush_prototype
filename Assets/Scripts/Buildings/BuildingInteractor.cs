using CastleRush.Interactions;
using CastleRush.SO.Building;
using CastleRush.UI.Popup;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace CastleRush.Buildings
{
    public class BuildingInteractor : BaseInteractor
    {
        [Inject] private PopupSystem _popupSystem;

        [SerializeField] private BuildingSetup setup;

        public override void Interaction()
        {
            OpenPanel();
        }

        private void OpenPanel()
        {
            Dictionary<int, object> parameters = new Dictionary<int, object>() { { 1, setup } };
            _popupSystem.Open(PopupType.Building, parameters);
        }
    }
}