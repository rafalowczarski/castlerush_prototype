﻿using CastleRush.Resources.Signals;
using CastleRush.SO.Resources;
using UnityEngine;
using Zenject;

namespace CastleRush.Buildings.Base
{
    internal class ResourceDrainer : MonoBehaviour,IBuildingElementInitializer
    {
        [Inject] private SignalBus _signalBus;
        private Resource _resource;

        public void Initialize(Resource resource)
        {
            this._resource = resource;
        }

        public void Invoke()
        {
            _signalBus.Fire(new RemoveResourceSignal(_resource));
        }
    }
}
