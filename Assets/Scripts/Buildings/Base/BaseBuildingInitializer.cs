﻿using CastleRush.SO.Resources;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CastleRush.Buildings.Base
{
    public abstract class BaseBuildingInitializer : MonoBehaviour
    {
        [SerializeField] protected Resource resource;

        public virtual void Initialize()
        {
            List<IBuildingElementInitializer> elements = GetComponents<IBuildingElementInitializer>().ToList();

            foreach (var element in elements)
            {
                element.Initialize(resource);
            }
        }
    }
}