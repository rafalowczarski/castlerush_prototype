﻿using CastleRush.Resources.Signals;
using CastleRush.SO.Resources;
using UnityEngine;
using Zenject;

namespace CastleRush.Buildings.Base
{
    public class ResourceGenerator : MonoBehaviour, IBuildingElementInitializer
    {
        [Inject] SignalBus _signalBus;
        private Resource _resource;

        public void Initialize(Resource resource)
        {
            _resource = resource;
        }

        public void Invoke()
        {
            _signalBus.Fire(new AddResourceSignal(_resource));
        }
    }
}
