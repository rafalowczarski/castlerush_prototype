﻿using CastleRush.SO.Resources;

namespace CastleRush.Buildings.Base
{
    public interface IBuildingElementInitializer
    {
        public void Initialize(Resource resource);
    }
}
