using CastleRush.Buildings.Signals;
using UnityEngine;
using Zenject;

namespace CastleRush.Buildings
{
    public class BuildingView : MonoBehaviour
    {
        [SerializeField] private Transform container;
        [Inject] private SignalBus _signalBus;
        [Inject] private DiContainer _diContainer;
        [Inject] private PlotRegistration _plotRegistration;
        public int PlotId { get; private set; }
        private void Awake()
        {
            PlotId = _plotRegistration.Register();
            _signalBus.Subscribe<OnBuildingSpawnRequestSignal>(OnBuildingSpawnRequest);
        }

        private void OnBuildingSpawnRequest(OnBuildingSpawnRequestSignal signal)
        {
            if (PlotId != signal.PlotId)
                return;

            for (int i = 0; i < container.childCount; i++)
            {
                Destroy(container.GetChild(i).gameObject);
            }
            GameObject building = _diContainer.InstantiatePrefab(signal.Setup.Model, container);

        }
    }
}