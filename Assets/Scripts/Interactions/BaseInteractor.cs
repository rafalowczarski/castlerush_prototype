﻿using UnityEngine;

namespace CastleRush.Interactions
{
    public interface IInteractor
    {
        void Interaction();
    }
    public abstract class BaseInteractor : MonoBehaviour, IInteractor
    {
        public abstract void Interaction();
    }
}
