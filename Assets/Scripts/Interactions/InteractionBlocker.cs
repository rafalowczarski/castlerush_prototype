﻿using UnityEngine;

namespace CastleRush.Interactions
{
    public interface IInteractorBlocker
    {
        public bool BlockRaycast { get; }
    }
    public class InteractionBlocker : MonoBehaviour, IInteractorBlocker
    {
        public bool BlockRaycast => blockRaycast;
        [SerializeField] private bool blockRaycast;
    }
}
