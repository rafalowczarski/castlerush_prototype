using Zenject;

namespace CastleRush.Input
{
    public class InputInstaller : MonoInstaller<InputInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<IInputService>().To<InputService>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<PointerInputProcesor>().AsSingle().NonLazy();
        }
    }
}