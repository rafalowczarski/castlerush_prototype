using CastleRush.Interactions;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CastleRush.Input
{
    public interface IInputService
    {
        public T TryGetComponentUnderCursor<T>();
    }
    public class InputService : IInputService
    {
        public T TryGetComponentUnderCursor<T>()
        {
            if (Time.timeScale == 0)
                return default(T);

            List<RaycastResult> results = CheckForStackOfObjectsWithRaycast();

            T component = default(T);

            for (int i = 0; i < results.Count; i++)
            {
                if (results[i].gameObject)
                {
                    IInteractorBlocker blocker = null;
                    results[i].gameObject.TryGetComponent<IInteractorBlocker>(out blocker);

                    if (blocker != null)
                    {
                        if (blocker.BlockRaycast)
                        {
                            break;
                        }
                    }

                    results[i].gameObject.TryGetComponent<T>(out component);
                    if (component != null)
                    {
                        break;
                    }
                }
            }
            return component;
        }
        private List<RaycastResult> CheckForStackOfObjectsWithRaycast()
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1,
            };
            pointerData.position = UnityEngine.Input.mousePosition;

            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);

            return results;
        }
    }
}