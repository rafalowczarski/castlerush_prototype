using CastleRush.Interactions;
using Zenject;

namespace CastleRush.Input
{
    public class PointerInputProcesor : ITickable
    {
        [Inject] IInputService _inputService;

        public void Tick()
        {
            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                IInteractor interactor = _inputService.TryGetComponentUnderCursor<IInteractor>();

                if (interactor != null)
                {
                    interactor.Interaction();
                }
            }
        }
    }
}